from flask import Flask, request
from flask_json import FlaskJSON, JsonError, json_response, as_json
import os

app = Flask(__name__)
# Don't add an extra "status" property to JSON responses - this would break the API contract
app.config['JSON_ADD_STATUS'] = False
# Don't sort properties alphabetically in response JSON
app.config['JSON_SORT_KEYS'] = False

json_app = FlaskJSON(app)


@json_app.invalid_json_error
def invalid_request_error(e):
    """Generates a valid ELG "failure" response if the request cannot be parsed"""
    raise JsonError(status_=400, failure={ 'errors': [
        { 'code':'elg.request.invalid', 'text':'Invalid request message' }
    ] })


@app.route('/process', methods=['POST'])
@as_json
def process_request():
    """Main request processing logic - accepts a JSON request and returns a JSON response."""
    data = request.get_json()
    # sanity checks on the request message
    if (data.get('type') != 'text') or ('content' not in data):
        invalid_request_error(None)

    content = data['content']
    result = do_nlp(content)
    return dict(response = { 'type':'annotations', 'annotations':result })


@app.before_first_request
def load_models():
    # load spacy sentence segmenter and tokenizer
    import spacy
    global snlp
    snlp = spacy.load('it_core_news_sm', disable=['ner'])
    #ADD A NEW RULE TO THE PIPELINE
    #from spacy.language import Language
    #@Language.component('newline_segmenter')
    #def newline_segmenter(doc):
    #    for token in doc[:-1]:
    #        if token.text == '\n':
    #            doc[token.i+1].is_sent_start = True
    #    return doc
    #snlp.add_pipe('newline_segmenter', before='parser')
    # load someweta pos tagger
    from someweta import ASPTagger
    global asptagger
    asptagger = ASPTagger()
    asptagger.load('data/models/spoken_italian_2021-02-26.model')


def do_nlp(content):
    #os.system('venv/bin/python tokenize_ita.py ' + "\"" + content + "\"")
    sentence_anns = []
    token_anns = []
    doc = snlp(content)
    for sent in doc.sents:
        sentence_anns.append({'start':sent.start_char, 'end':sent.end_char})
        sent_tokens = [tok.text for tok in sent];
        tagged_tokens = asptagger.tag_sentence(sent_tokens)
        for tok, tagged in zip(sent, tagged_tokens):
            token_anns.append({'start':tok.idx, 'end':(tok.idx+len(tok)), 'features':{'tag':tagged[1]} })
    
    return {'Sentence':sentence_anns, 'Token':token_anns}

if __name__ == '__main__':
    app.run()
