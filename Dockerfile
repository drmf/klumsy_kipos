FROM python:3.6

COPY ./ /elg/

WORKDIR /elg

RUN python -m venv venv && venv/bin/pip install --upgrade pip && venv/bin/pip --no-cache-dir install -r requirements.txt && venv/bin/python -m spacy download it_core_news_sm

ENV WORKERS=1

ENTRYPOINT ["./docker-entrypoint.sh"]
